/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------GRILLE----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
var table = [
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0]
];


/*Variables des jetons*/

var count_switch = 1;
var redTokenHTML = document.querySelector(".tokR").innerHTML;
var yellowTokenHTML = document.querySelector(".tokY").innerHTML;
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------FORMULAIRE-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

//On recupère le nom des joueurs

function nomDesJoueurs() {
  var nom1 = document.getElementById("p1").value;
  var nom2 = document.getElementById("p2").value;
  document.getElementById("joueur1").innerHTML = nom1;
  document.getElementById("joueur2").innerHTML = nom2;
  document.getElementById("formPlayers").style.display = "none";
}


/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------REGLES---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//apparition et disparition des règles écrites du jeu 

function rules(){
  var a = document.getElementById("apparitionRegles");
  a.classList.toggle("regles");
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------FONCTION ARROW()+CHANGEMENT DE COULEUR DES JETONS-------------------------------------------------------------------------------------------------------------------(cf. README.md)-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//fonction qui permet de placer un jeton lorsqu'on clic sur l'icone flèche

function arrowclick(n) {
  var column = table[n]; //   [0, 0, 0, 0, 0, 0, 0],

  // parcours la colonnes jusqu'à la limite

  for (var i = 0; i < column.length; i++) {

   
    var box = column[i];
    if (box === 0) {                                                 //verifie si la case est vide
      table[n][i] = 1;                                              
        if(count_switch){                                           
            // html
            var boxHTML =  document.querySelector('.col-'+n+'-'+i);
            boxHTML.innerHTML = redTokenHTML;                             //ajout du jeton rouge
            document.querySelector("#joueur1").classList.remove("J1");
            document.querySelector("#joueur2").classList.add("J2");      
            
            count_switch=0;                                               //compteur qui sert à alterner la couleur en rouge
            console.log("red"+count_switch);
            countTokens();
        }else{                                                    
            var boxHTML =  document.querySelector('.col-'+n+'-'+i);
            boxHTML.innerHTML = yellowTokenHTML;                         //ajout du jeton jaune
             document.querySelector("#joueur2").classList.remove("J2");
            document.querySelector("#joueur1").classList.add("J1");
            count_switch=1;                                        //compteur qui sert à alterner la couleur en jaune
            console.log("yellow"+count_switch);
            countTokens();
        }
      break;
    }
  }

  // apres la boucle on vérifie si la colonne est pleine
  if (i === column.length) {                                        
    alert('La colonne est compléte');
  }


  function countTokens() {

    var nom1 = document.getElementById("p1").value;
    var nom2 = document.getElementById("p2").value;  //on récupère le nom des joueurs pour annoncé les gagnants
    
        var countTR = 0;
        var countTY = 0;      //on initialise le nombre de jetons rouge/jaune alignés en colonne
    
        for (var j = 0; j<column.length; j++) {
  
        var box = column[j];
        var boxHTML =  document.querySelector('.col-'+n+'-'+j);  

        if(boxHTML.innerHTML == redTokenHTML) {  //On vérifie si dans la case il y a un jeton rouge
  
          countTR++;                              // si oui, on incrémente les point pour le rouge
          console.log("countTR = " + countTR);
    
          if (countTR == 4) {                     //si on a 4 jetons alignés, on gagne
            alert("Congragtulation " + nom1 + " you win!");
          }
          else if(i == column.length && countTR!= 4) {
            countTR = 0;
          }
        }
        // si le compteur de jetons n'arrive pas à 4 on réinitilise
    
        if ( boxHTML.innerHTML == yellowTokenHTML) {
          countTY++;
          console.log("countTY= " + countTY);
          if (countTY == 4) {
            alert("Congragtulation " + nom2 + " you win!");
          }
          else if(i == column.length && countTY!= 4) {
            countTY = 0;
          }
          
        };
        
    
      };
  }
}
