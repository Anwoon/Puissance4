/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------PUISSANCE4-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

EXPLICATIONS DES FONCTIONS:
---------------------------


1) function arrowclick(n):

Cette fonction sert à insérer les jetons dans la grille tout en verifiant la présence de jetons dans la colone selectionner.

La boucle for vérifie la présence d'un jeton à l'index [0] et incrémente si celui-ci est déjà occupé jusqu'à en trouver un de libre .
Si l'index est libre , alors un jeton est placé. La couleur du jeton est déterminé par la valeur de la variable count_switch, soit true soit false (1 ou 0). Une fois le jeton placé , count_switch prend la valeur opposée.

Si ils sont tous occupés, la boucle for se stop et une boucle if compare i à la "longueur" de la colonne . Si la valeur de i est égale à la longueur de la colonne alors aucun jeton de plus ne sera placé et le joueur est averti de son erreur .




2) fonction nomDesJoueurs() :

Cette fonction sert à récupérer les valeurs(noms des joueurs) des inputs et les affiches sur la page du jeu.

les variables nom1 et nom2 sont les valeurs récupérées dans les inputs text.
Elles sont ajoutées aux éléments #joueur1 et #joueur2 grace à getElementById().innerHTML.

Le formulaire disparait une fois les entrées utilisateurs validées en utilisant la propriété css display = "none".




3) fonction rules() : 

On fait disparaitre les regles du jeu en ajoutant ou en retirant une classe css qui modifie l'opacité du texte grace à la methode toggle("").

